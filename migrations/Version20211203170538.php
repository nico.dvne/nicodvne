<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Maintenance / Prod version generic parameter
 */
final class Version20211203170538 extends AbstractMigration
{

    private const TABLE_NAME = 'website_parameters';

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `website_parameters`(`name`, `description`, `value`) VALUES ('Switch Maintenance','parametre switch prod ou maintenance','NON');");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `website_parameters` WHERE 'name' = 'Switch Maintenance';");
    }
}
