<?php

namespace App\Controller;

use App\Entity\WebsiteParameters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        $websiteParametersRepo = $this->entityManager->getRepository(WebsiteParameters::class);
        if (!$websiteParametersRepo->isProdModActivated()) {
            return $this->render('waiting.html.twig', [
                'controller_name' => 'DefaultController',
            ]);
        }

        return $this->render('index.html.twig', [
            'controller_name' => 'DefaultController'
        ]);
    }
}
