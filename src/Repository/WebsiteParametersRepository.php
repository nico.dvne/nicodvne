<?php

namespace App\Repository;

use App\Entity\WebsiteParameters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WebsiteParameters|null find($id, $lockMode = null, $lockVersion = null)
 * @method WebsiteParameters|null findOneBy(array $criteria, array $orderBy = null)
 * @method WebsiteParameters[]    findAll()
 * @method WebsiteParameters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebsiteParametersRepository extends ServiceEntityRepository
{
    public const SWITCH_PROD_MAINTENANCE = "Switch Maintenance";

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebsiteParameters::class);
    }

    /**
     * @return WebsiteParameters|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getProdModEntity(): ?WebsiteParameters
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.name = :val')
            ->setParameter('val', self::SWITCH_PROD_MAINTENANCE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isProdModActivated(): bool
    {
        return "OUI" === $this->getProdModEntity()->getValue();
    }

    // /**
    //  * @return WebsiteParameters[] Returns an array of WebsiteParameters objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WebsiteParameters
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
